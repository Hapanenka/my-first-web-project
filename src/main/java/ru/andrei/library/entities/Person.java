package ru.andrei.library.entities;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;


@Entity
@Table(name = "Person")
public class Person {
    @Id
    @Column(name = "person_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int person_id;


    @NotEmpty(message = "Name should not be empty")
    @Size(min = 2, max = 40, message = "Name size from 2 to 40 characters")
    @Column(name = "name")
    private String name;

    // @Pattern(regexp = "\\d{4}", message = "You need to enter the year of birth in the format 1999")
    @Min(value = 1900, message = "You should specify the real year of birth")
    @Max(value = 2100, message = "You should specify the real year of birth")
    @Column(name = "year")
    private int year;   // year of birth

    @OneToMany(mappedBy = "owner")
    private List<Book> book;

    public Person() {
    }

    public int getPerson_id() {
        return person_id;
    }

    public void setPerson_id(int person_id) {
        this.person_id = person_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Person(String name, int year) {
        this.name = name;
        this.year = year;
    }

    public Person(int person_id, String name, int year) {
        this.person_id = person_id;
        this.name = name;
        this.year = year;
    }

    public List<Book> getBook() {
        return book;
    }

    public void setBook(List<Book> book) {
        this.book = book;
    }

}
