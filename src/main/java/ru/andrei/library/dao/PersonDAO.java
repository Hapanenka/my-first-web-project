/*
package ru.andrei.library.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.andrei.library.entities.Book;
import ru.andrei.library.entities.Person;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class PersonDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public PersonDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional(readOnly = true)
    public List<Person> index() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("select p from Person p", Person.class)
                .getResultList();
    }

    @Transactional(readOnly = true)
    public Person show(int person_id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Person.class, person_id);
    }

    @Transactional
    public void save(Person person) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(person);
    }

    @Transactional
    public void update(int id, Person updatedPerson) {
        Session session = sessionFactory.getCurrentSession();
        Person personToBeUpdated = session.get(Person.class, id);

        personToBeUpdated.setName(updatedPerson.getName());
        personToBeUpdated.setYear(updatedPerson.getYear());
    }

    @Transactional
    public void delete(int id) {
        Session session = sessionFactory.getCurrentSession();
        session.remove(session.get(Person.class, id));
    }


        public Optional<Person> show(String name) {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("select p from Person p", Person.class)
                       .getResultList();


         return jdbcTemplate.query("SELECT * FROM Person WHERE name=?", new Object[] {name},
                 new BeanPropertyRowMapper<>(Person.class)).stream().findAny();
    }

    @Transactional(readOnly = true)
    public List<Book> getBooksByPersonId(int person_id) {
        Session session = sessionFactory.getCurrentSession();

        List<Book> books = session.createQuery("select b from Book b left join fetch b.owner")
                .getResultList();

        return books;
          return jdbcTemplate.query("SELECT * FROM Book WHERE person_id = ?", new Object[]{person_id},
                  new BeanPropertyRowMapper<>(Book.class));
    }
}*/
