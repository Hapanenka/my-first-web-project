/*
package ru.andrei.library.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.andrei.library.entities.Book;
import ru.andrei.library.entities.Person;

import java.util.List;
import java.util.Optional;

@Component
public class BookDAO {
    private final SessionFactory sessionFactory;

    @Autowired
    public BookDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    public void delete(int book_id) {
        Session session = sessionFactory.getCurrentSession();
        session.remove(session.get(Book.class, book_id));
    }

    @Transactional(readOnly = true)
    public List<Book> index() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("SELECT p FROM Book p", Book.class)
                .getResultList();
    }

    @Transactional
    public Book show(int book_id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Book.class, book_id);
    }

    @Transactional
    public void save(Book book) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(book);
    }

    @Transactional
    public void update(int book_id, Book updatedBook) {
        Session session = sessionFactory.getCurrentSession();
        Book bookToBeUpdated = session.get(Book.class, book_id);

        bookToBeUpdated.setName(updatedBook.getName());
        bookToBeUpdated.setAuthor(updatedBook.getAuthor());
        bookToBeUpdated.setYear(updatedBook.getYear());
    }

public Optional<Person> getBookOwner(int book_id) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("select o from Book o WHERE :value is null or o.person_id = :value").stream().findAny();


        return jdbcTemplate.query("SELECT Person.* FROM Book JOIN Person ON Book.person_id = Person.person_id " +
        "WHERE Book.book_id = ?", new Object[]{book_id}, new BeanPropertyRowMapper<>(Person.class)).stream().findAny();
    }

    public void giveBook(int book_id, Person selectPerson) {
        Session session = sessionFactory.getCurrentSession();
        Book bookToBeUpdated = session.get(Book.class, book_id);

        bookToBeUpdated.setPerson_id(selectPerson.getPerson_id());
       jdbcTemplate.update("UPDATE Book SET person_id=? WHERE book_id=?", selectPerson.getPerson_id(), book_id);
    }

    public void release(int book_id) {
        Session session = sessionFactory.getCurrentSession();
        Book bookToBeUpdated = session.get(Book.class, book_id);
        bookToBeUpdated.setPerson_id(0);
     jdbcTemplate.update("UPDATE Book SET person_id=NULL WHERE book_id=?", book_id);
    }


 public Optional<Book> show(String name) {
        return jdbcTemplate.query("SELECT * FROM Book WHERE name=?", new Object[]{name},
                new BeanPropertyRowMapper<>(Book.class)).stream().findAny();
    }



}
*/
