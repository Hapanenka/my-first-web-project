package ru.andrei.library.controllers;



import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import ru.andrei.library.entities.Book;
import ru.andrei.library.entities.Person;
import ru.andrei.library.services.BooksServices;
import ru.andrei.library.services.PeopleServices;

import javax.validation.Valid;


@Controller
@RequestMapping("/book")
public class BookControllers {

    private final BooksServices booksServices;
    private final PeopleServices peopleServices;

    public BookControllers(BooksServices booksServices, PeopleServices peopleServices) {
        this.booksServices = booksServices;
        this.peopleServices = peopleServices;
    }

    @GetMapping()
    public String index(Model model, @RequestParam(value = "page", required = false) Integer page,
                        @RequestParam(value = "book_per_page", required = false) Integer bookPerPage,
                        @RequestParam(value = "sort_by_year", required = false) boolean sortByYear) {

        if (page == null || bookPerPage == null)
            model.addAttribute("book", booksServices.findAll(sortByYear)); // выдача всех книг
        else
            model.addAttribute("book", booksServices.findWithPagination(page, bookPerPage, sortByYear));

        return "book/index";
    }

    @GetMapping("/{id}")
    public String show(@PathVariable("id") int id, Model model, @ModelAttribute("person") Person person) {
        model.addAttribute("book", booksServices.findOne(id));
        Person bookOwner = booksServices.getBookOwner(id);
        if (bookOwner != null)
            model.addAttribute("owner", bookOwner);
        else
            model.addAttribute("people", peopleServices.findAll());

        return "book/show";
    }

    @GetMapping("/new")
    public String newBook(@ModelAttribute("book") Book book) {
        return "book/new";
    }

    @PostMapping()
    public String create(@ModelAttribute("book") @Valid Book book,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return "book/new";
        booksServices.save(book);
        return "redirect:/book";
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @PathVariable("id") int book_id) {
        model.addAttribute("book", booksServices.findOne(book_id));
        return "book/edit";
    }

    @PatchMapping("/{id}")
    public String update(@ModelAttribute("book") @Valid Book book, BindingResult bindingResult,
                         @PathVariable("id") int id) {
        if (bindingResult.hasErrors())
            return "book/edit";

        booksServices.update(id, book);
        return "redirect:/book";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        booksServices.delete(id);
        return "redirect:/book";
    }

    @PatchMapping("/{id}/giveBook")
    public String assign(@PathVariable("id") int id, @ModelAttribute("person") Person selectPerson) {
        booksServices.assign(id, selectPerson);
        return "redirect:/book";
    }

    @PatchMapping("/{id}/release")
    public String release(@PathVariable("id") int book_id) {
        booksServices.release(book_id);
        return "redirect:/book";
    }

    @PostMapping("/search")
    public String makeSearch(Model model, @RequestParam("query") String query) {
        model.addAttribute("book", booksServices.searchByTitle(query));
        return "book/search";
    }

    @GetMapping("/search")
    public String searchPage() {
        return "book/search";
    }
}